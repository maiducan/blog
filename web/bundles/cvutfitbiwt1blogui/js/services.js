/**
 * Created by ducan on 19.05.2016.
 */

//---------------------------------------------------------------------------------------------------------------------//
var restServices = angular.module('restServices', ['ngResource']);
var responseServices = angular.module('responseServices', []);


//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetPosts', ['$resource',
    function($resource){
        return $resource('/api/posts.json', {}, {
            query: {method:'GET', isArray:true}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetPost', ['$resource',
    function($resource){
        return $resource('/api/posts/:postID.json', {}, {
            get: {
                method:'GET', params:{postID:'postID'}
            }
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('NewPost', ['$resource',
    function($resource){
        return $resource('/api/posts.json?apikey=42', {}, {
            save: {
                method: 'POST'
            }
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('EditPost', ['$resource',
    function($resource){
        return $resource('/api/posts/:postID.json', {}, {
            save: {method:'PUT', params:{postID:'posts'}}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('DeletePost', ['$resource',
    function($resource){
        return $resource('/api/posts/:postID.json', {}, {
            delete: {method:'DELETE', params:{postID:'posts'}}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetComments', ['$resource',
    function($resource){
        return $resource('/api/comments.json', {}, {
            query: {method:'GET', isArray:true}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetComment', ['$resource',
    function($resource){
        return $resource('/api/comments/:postID.json', {}, {
            query: {
                method:'GET', isArray:true, params:{postID:'postID'}
            }
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('NewComment', ['$resource',
    function($resource){
        return $resource('/api/comments/:postID.json', {}, {
            save: {
                method: 'POST', params:{postID:'posts'}
            }
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('EditComment', ['$resource',
    function($resource){
        return $resource('/api/comments/:commentID.json', {}, {
            save: {method:'PUT', params:{commentID:'comments'}}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('DeleteComment', ['$resource',
    function($resource){
        return $resource('/api/comments/:commentID.json', {}, {
            delete: {method:'DELETE', params:{commentID:'comments'}}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetFiles', ['$resource',
    function($resource){
        return $resource('/api/files/:postID.json', {}, {
            query: {method:'GET', isArray:true}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('AddFile', ['$resource',
    function($resource){
        return $resource('/api/files/:postID.json', {}, {
            save: {
                method: 'POST', params:{postID:'posts'}
            }
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('DeleteFile', ['$resource',
    function($resource){
        return $resource('/api/files/:fileID.json', {}, {
            delete: {method:'DELETE', params:{fileID:'files'}}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetUser', ['$resource',
    function($resource){
        return $resource('/api/user.json', {}, {
            get: {method:'GET'}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//
restServices.factory('GetUsers', ['$resource',
    function($resource){
        return $resource('/api/users.json', {}, {
            query: {method:'GET', isArray:true}
        });
    }
]);
//---------------------------------------------------------------------------------------------------------------------//


responseServices.factory('myHttpInterceptor',

    function($q, $location, $injector, $window){
        return {
            'response': function(response){

                var Notification = $injector.get('Notification');

                if (typeof response.data === 'string' && response.data.indexOf("Neboj se vyplnit")>-1) {

                    Notification.info('Přihlašte se prosím :)');
                    $window.location.href = "/login";

                    return $q.reject(response);

                }
                else {

                    return response;

                }

            }

        };
    }
);



