(function() {
    //---------------------------------------------------------------------------------------------------------------------//
    var app = angular.module('blog', [
        'ngRoute',
        'postControllers',
        'commentControllers',
        'fileControllers',
        'userControllers',
        'restServices',
        'responseServices',
        'ui-notification',
        'angular-loading-bar'
    ]);

    // CONFIGURATION ---------------------------------------------------------------------------------------------------------------------//

    // Template chars {$ ... $}
    app.config(function($interpolateProvider){
            $interpolateProvider.
            startSymbol('{$').
            endSymbol('$}');
        });

    // Notifications
    app.config(function(NotificationProvider) {
            NotificationProvider.setOptions({
                delay: 3000,
                startTop: 50,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'bottom'
        });
    });

    // Routing
    app.config(function($routeProvider) {
            $routeProvider.
            when('/posts', {
                templateUrl: '/bundles/cvutfitbiwt1blogui/templates/posts/post-list.html',
                controller: 'PostController'
            }).
            when('/posts/:postID', {
                templateUrl: '/bundles/cvutfitbiwt1blogui/templates/posts/post-detail.html',
                controller: 'PostDetailController'
            }).
            when('/post/new', {
                templateUrl: '/bundles/cvutfitbiwt1blogui/templates/posts/post-new.html',
                controller: 'PostNewController'
            }).
            when('/post/edit/:postID', {
                templateUrl: '/bundles/cvutfitbiwt1blogui/templates/posts/post-edit.html',
                controller: 'PostEditController'
            }).
            when('/post/delete/:postID', {
                templateUrl: '/bundles/cvutfitbiwt1blogui/templates/posts/post-list.html',
                controller: 'PostDeleteController'
            }).
            otherwise({
                redirectTo: '/posts'
            });
        });

    // Response provider
    app.config(function($httpProvider) {
        $httpProvider.interceptors.push('myHttpInterceptor');
    });

    // DIRECTIVES ---------------------------------------------------------------------------------------------------------------------//

    // Post short
    app.directive('postShort', function() {
        return {
            templateUrl: '/bundles/cvutfitbiwt1blogui/templates/directives/post-short.html'
        };
    });

    // Comments box
    app.directive('commentsBox', function() {

        return {
            templateUrl: '/bundles/cvutfitbiwt1blogui/templates/directives/comments-box.html'
        };

    });

    // Files box
    app.directive('filesBox', function() {

        return {
            templateUrl: '/bundles/cvutfitbiwt1blogui/templates/directives/files-box.html'
        };

    });

    // Form box
    app.directive('formBox', function() {

        return {
            templateUrl: '/bundles/cvutfitbiwt1blogui/templates/directives/form-box.html'
        };

    });

    // FILTERS ---------------------------------------------------------------------------------------------------------------------//

    // Paging
    app.filter('startFrom', function() {
        return function(input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
    });

    // Publish date filtering
    app.filter('dateFilter', function() {

        return function(post) {

            var retArray = [];
            var today = new Date();

            angular.forEach(post, function(obj){

                var publishFrom = new Date(obj.publish_from);
                var publishTo = new Date(obj.publish_to);

                if(today >= publishFrom && today <= publishTo) {
                    retArray.push(obj);
                }
            });

            return retArray;
        };
    });

    app.filter('searchFilter', function() {

        return function(post, author) {

            var retArray = [];

            angular.forEach(post, function(obj){

                if (obj == author.name)
                    retArray.push(obj);

            });

            return retArray;
        };


    });

}) ();
