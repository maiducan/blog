/**
 * Created by ducan on 22.04.2016.
 */


function postShow() {

    $("div.posts").fadeIn(500);


}

function postDetail() {

    $("a.detail").click(function(){

        $('div.modal-fade').fadeIn(50);
        $('div.modal').fadeIn(200);

    });

    $("div.modal-fade").click(function(){

        $('div.modal-fade').fadeOut()
        $('div.modal').fadeOut(50);

    });

}



$(document).ready(function (){

    /* Post effects */
    postShow();

    /* Post detail show and hide detail */
    postDetail();


});
