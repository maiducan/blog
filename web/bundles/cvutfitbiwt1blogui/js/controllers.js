/**
 * Created by ducan on 18.05.2016.
 */

var postControllers = angular.module('postControllers', []);
var commentControllers = angular.module('commentControllers', []);
var fileControllers = angular.module('fileControllers', []);
var userControllers = angular.module('userControllers', []);

//---------------------------------------------------------------------------------------------------------------------//
postControllers.controller('PostController', ['$scope', 'GetPosts', 'GetUsers',
    // Get data from data.js //
    function ($scope, GetPosts, GetUsers) {
        
        $scope.posts = GetPosts.query();
        $scope.authors = GetUsers.query();

        $scope.currentPage = 0;
        $scope.pageSize = 3;

        $scope.openSearch = function() {

            $scope.searchForm = !$scope.searchForm;


        };

    }

]);
//---------------------------------------------------------------------------------------------------------------------//
postControllers.controller('PostDetailController', ['$scope', '$routeParams', '$location', 'GetPost', 'GetComment', 'GetFiles', 'Notification',

    function ($scope, $routeParams, $location, GetPost, GetComment, GetFiles, Notification) {

        var id = $routeParams.postID;

        $scope.currentPage = 0;
        $scope.pageSize = 3;
        
        $scope.post = GetPost.get({postID: id});
        $scope.comments = GetComment.query({postID: id});
        $scope.files = GetFiles.query({postID:id});

    }
]);
//---------------------------------------------------------------------------------------------------------------------//
postControllers.controller('PostNewController', ['$scope', '$location', 'NewPost', 'Notification',

    function($scope, $location, NewPost, Notification) {

        $scope.post = new NewPost;
        $scope.post.private = false;
        $scope.post.publishFrom = new Date();
        $scope.post.publishTo = new Date();


        $scope.addPost = function() {

            NewPost.save($scope.post).$promise.then(function() {
                //this will be fired upon success

                Notification.info('Článek zveřejněn');
                $location.path('/posts');

            }).catch(function (response) {

                responseHandle(response.status, $location, Notification);

            });


        }


    }


]);
//---------------------------------------------------------------------------------------------------------------------//
postControllers.controller('PostEditController', ['$scope', '$routeParams', '$location', 'EditPost', 'GetPost', 'Notification',

    function ($scope, $routeParams, $location, EditPost, GetPost, Notification) {

        var id = $routeParams.postID;

        $scope.post = {};
             
        GetPost.get({postID: id}, function(post) {

            $scope.post.title = post.title;
            $scope.post.text = post.text;
            $scope.post.private = post.private;
            $scope.post.publishFrom = new Date(post.publish_from);
            $scope.post.publishTo = new Date(post.publish_to);


        });

        $scope.editPost = function() {

            console.log($scope.post);

            EditPost.save({postID: id}, $scope.post).$promise.then(function() {
                //this will be fired upon success

                Notification.info('Článek upraven');
                $location.path('/posts/' + id);

            }).catch(function (response) {

                responseHandle(response.status, $location, Notification);

            });


        }


    }


]);
//---------------------------------------------------------------------------------------------------------------------//
postControllers.controller('PostDeleteController', ['$routeParams', '$location', 'DeletePost', 'Notification',

    function($routeParams, $location, DeletePost, Notification) {

        var id = $routeParams.postID;


        DeletePost.delete({postID: id}).$promise.then(function() {
            //this will be fired upon success

            Notification.info('Článek smazán');
            $location.path('/posts');

        }).catch(function (response) {

            responseHandle(response.status, $location, Notification);

        });



    }

]);
//---------------------------------------------------------------------------------------------------------------------//
commentControllers.controller('CommentNewController', ['$routeParams', '$scope', '$location', '$route', 'NewComment', 'GetPost', 'Notification',
    function ($routeParams, $scope, $location, $route, NewComment, GetPost, Notification) {

        var $postID = $routeParams.postID;

        $scope.currentPage = 0;
        $scope.pageSize = 5;

        $scope.addComment = function () {

            var comment = angular.copy($scope.comment);


            NewComment.save({postID: $postID}, comment).$promise.then(function() {
                //this will be fired upon success

                Notification.info('Komentář přidán');
                $location.path('/posts/' + $postID);
                $route.reload();

            }).catch(function(response) {
                //this will be fired upon error

                responseHandle(response.status, $location, Notification);

            });


            $scope.comment.text = "";

        }


    }

]);
//---------------------------------------------------------------------------------------------------------------------//
commentControllers.controller('CommentEditController', ['$scope', '$routeParams', '$location', 'EditComment', 'GetPost', 'Notification',

    function ($scope, $routeParams, $location, EditComment, GetPost, Notification) {

        var postId = $routeParams.postID;

        $scope.editCommentField = {'visibility': 'hidden'};


        $scope.editComment = function() {

            var commentId = $scope.comment.id;

            console.log('sssssssssssssss');

            console.log($scope.comment.text);

            $scope.editCommentField = {'visibility': 'visible'};

        };

        $scope.updateComment = function() {

            var commentId = $scope.comment.id;
            $scope.editCommentField = {'visibility': 'hidden'};

            EditComment.save({commentID: commentId}, $scope.comment).$promise.then(function() {
                //this will be fired upon success

                Notification.info('Komentář upraven');

            }).catch(function(response) {
                //this will be fired upon error

                responseHandle(response.status, $location, Notification);

            });


        }


    }


]);
//---------------------------------------------------------------------------------------------------------------------//
commentControllers.controller('CommentDeleteController', ['$routeParams', '$scope', '$location', '$route' , 'DeleteComment', 'Notification',

    function($routeParams, $scope, $location, $route, DeleteComment, Notification) {

        var postId = $routeParams.postID;


        $scope.deleteComment = function (){

            var commentid = $scope.comment.id;

            DeleteComment.delete({commentID: commentid}).$promise.then(function (response) {

                Notification.info('Komentář smazán');
                $location.path('/posts/' + postId);
                $route.reload();

            }).catch(function (response) {

                responseHandle(response.status, $location, Notification);

            });

        };



    }

]);
//---------------------------------------------------------------------------------------------------------------------//
fileControllers.controller('FileNewController', ['$routeParams', '$scope', '$location', '$route', 'AddFile', 'GetPost', 'Notification',
    function ($routeParams, $scope, $location, $route, AddFile, GetPost, Notification) {

        var $postID = $routeParams.postID;

        $scope.currentPage = 0;
        $scope.pageSize = 5;

        $scope.addFile = function () {

            var file = angular.copy($scope.comment);


            AddFile.save({postID: $postID}, comment).$promise.then(function() {
                //this will be fired upon success

                Notification.info('Soubor přidán');
                $location.path('/posts/' + $postID);
                $route.reload();

            }).catch(function(response) {
                //this will be fired upon error

                responseHandle(response.status, $location, Notification);

            });



        }


    }

]);
//---------------------------------------------------------------------------------------------------------------------//
fileControllers.controller('FileDeleteController', ['$routeParams', '$scope', '$location', '$route' , 'DeleteFile', 'Notification',

    function($routeParams, $scope, $location, $route, DeleteFile, Notification) {

        var postId = $routeParams.postID;

        $scope.deleteFile = function (){

            var fileid = $scope.file.id;

            DeleteFile.delete({fileID: fileid}).$promise.then(function (response) {

                Notification.info('Soubor smazán');
                $location.path('/posts/' + postId);
                $route.reload();

            });

        };



    }

]);
//---------------------------------------------------------------------------------------------------------------------//

userControllers.controller('UserController', ['$scope', 'GetUser',
    function ($scope, GetUser) {

        this.user = GetUser.get();

    }

]);
//---------------------------------------------------------------------------------------------------------------------//


var responseHandle = function(statusCode, $location, Notification) {


    switch (statusCode) {

        case 302: Notification.info('Musíte se přihlásit'); break;
        case 403: Notification.warning('Nemáte dostatečná oprávnění'); $location.path('/posts'); break;
        case 500: Notification.error('Chyba na serveru'); break;


    }

}
