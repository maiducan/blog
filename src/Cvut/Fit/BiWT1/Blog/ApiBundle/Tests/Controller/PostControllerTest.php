<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 06.04.2016
 * Time: 22:21
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{

    private $client = null;
    private $crawler = null;

    function __construct() {

        $this->client = static::createClient();

    }

    public function testResponseCode()
    {


        $this->crawler = $this->client->request('GET', '/api/posts.xml');
        $status = $this->client->getResponse()->getStatusCode();
        $this->assertEquals(200, $status);

    }

    public function testContent() {

        $this->crawler = $this->client->request('GET', '/api/posts.xml');
        $id = $this->crawler->filter('id')->last()->text();
        $this->assertEquals(177, $id);

    }

}