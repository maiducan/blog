<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 21.05.2016
 * Time: 8:58
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Security\CommentVoter;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\CommentType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class CommentController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all comments
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cgetAction() {

        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $comments = $commentFunc->findAll();

        $view = $this->view($comments, 200);
        return $this->handleView($view);

    }

    /**
     * Get comments of specified post
     * @param $postID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction($postID) {

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($postID);

        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        //$parent = $commentFunc->findById($id);
        $comments = $commentFunc->findByPost($post);


        $view = $this->view($comments, 200);
        return $this->handleView($view);

    }

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Create new comment at post with specific ID"
     * )
     *
     * @param Request $request
     * @param $postID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(Request $request, $postID) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $comment = new Comment();

        /* Voter authorization */
        $this->denyAccessUnlessGranted(CommentVoter::COMMENT_CREATE, $comment, 'Nemáte oprávnění psát komentáře!');

        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        //$parent = $commentFunc->findById($id);
        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($postID);

        $content = $request->getContent();
        $params = json_decode($content, true);


        $comment->setAuthor($this->getUser());
        //$comment->setParent($parent);
        $comment->setText($params['text']);
        $comment->setPost($post);
        $comment->setCreated(new \DateTime());
        $comment->setModified(new \DateTime());

        // Through form - not working, maybe in future
        /*
                $form = $this->createForm(new CommentType(), $comment);
                $form->handleRequest($request);

                if($form->isSubmitted())
                {

                    if($form->isValid())
                    {
                        $commentFunc->create($comment);
                        echo '44444';
                    }
                }*/

        $commentFunc->create($comment);

        $view = $this->view($comment, 201);
        return $this->handleView($view);
    }

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Edit a comment with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the comment"
     *      }
     *  }
     * )
     *
     * @param Request $request
     * @param $commentID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putAction(Request $request, $commentID) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $comment = $commentFunc->findById($commentID);

        /* Voter authorization */
        $this->denyAccessUnlessGranted(CommentVoter::COMMENT_EDIT, $comment, 'Nemáte oprávnění upravovat komentáře!');

        $content = $request->getContent();
        $params = json_decode($content, true);

        $comment->setText($params['text']);
        $comment->setModified(new \DateTime());

        $commentFunc->update($comment);

        $view = $this->view($comment, 200);
        return $this->handleView($view);

    }

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Delete comment with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the comment"
     *      }
     *  }
     * )
     *
     * @param $commentID
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function deleteAction($commentID) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $comment = $commentFunc->findById($commentID);

        /* Voter authorization */
        $this->denyAccessUnlessGranted(CommentVoter::COMMENT_EDIT, $comment, 'Nemáte oprávnění upravovat komentáře!');

        $commentFunc->delete($comment);

        $view = $this->view(null, 204);
        return $this->handleView($view);


    }

}