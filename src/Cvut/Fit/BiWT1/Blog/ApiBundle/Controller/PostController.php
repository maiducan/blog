<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 01.04.2016
 * Time: 9:47
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Security\PostVoter;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\FilterType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS;
use Symfony\Component\Validator\Constraints\Date;


class PostController extends FOSRestController implements ClassResourceInterface
{

    /**
     * Get all posts
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Returns all posts in database"
     * )
     */
    public function cgetAction() {

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts = $postOperation->getAllPosts();

        $view = $this->view($posts, 200);
        return $this->handleView($view);
    }

    /**
     * Get post with ID
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return post with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction($id) {

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($id);


        /* Voter authorization */
        $this->denyAccessUnlessGranted(PostVoter::POST_VIEW, $post, 'Nemáte oprávnění prohlížet tento příspěvek!');


        $view = $this->view($post, 200);
        return $this->handleView($view);

    }

    /**
     * Creates new POST
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Create new post"    
     * )
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(Request $request) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $post = new Post();

        /* Voter authorization */
        $this->denyAccessUnlessGranted(PostVoter::POST_CREATE, $post, 'Nemáte oprávnění vytvářet příspěvky!');

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');

        $content = $request->getContent();
        $params = json_decode($content, true);

        $post->setAuthor($this->getUser());
        $post->setTitle($params['title']);
        $post->setText($params['text']);
        $post->setPublishFrom(new \DateTime($params['publishFrom']));
        $post->setPublishTo(new \DateTime($params['publishTo']));
        $post->setPrivate($params['private']);
        $post->setCreated(new \DateTime());
        $post->setModified(new \DateTime());

        $postOperation->create($post, [], []);

        $view = $this->view(null, 200);
        return $this->handleView($view);

    }

    /**
     * Edit post
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Edit a post with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putAction(Request $request, $id) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($id);

        /* Voter authorization */
        $this->denyAccessUnlessGranted(PostVoter::POST_EDIT, $post, 'Nemáte oprávnění upravovat tento příspěvek!');

        $content = $request->getContent();
        $params = json_decode($content, true);

        $post->setTitle($params['title']);
        $post->setText($params['text']);
        $post->setPublishFrom(new \DateTime($params['publishFrom']));
        $post->setPublishTo(new \DateTime($params['publishTo']));
        $post->setPrivate($params['private']);
        $post->setModified(new \DateTime());

        $postOperation->update($post);

        $view = $this->view($post, 200);
        return $this->handleView($view);
    }

    /**
     * Delete post
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Delete post with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($id);

        /* Voter authorization */
        $this->denyAccessUnlessGranted(PostVoter::POST_EDIT, $post, 'Nemáte oprávnění upravovat tento příspěvek!');

        $postOperation->delete($post);

        $view = $this->view(null, 204);
        return $this->handleView($view);

    }


}