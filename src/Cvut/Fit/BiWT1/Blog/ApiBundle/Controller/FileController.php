<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 01.06.2016
 * Time: 7:09
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\File;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class FileController extends FOSRestController implements ClassResourceInterface
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Return files with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     * Get files of specified post
     * @param $postID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cgetAction($postID) {

        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($postID);

        $files = $post->getFiles();

        $view = $this->view($files, 200);
        return $this->handleView($view);

    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Add new file to specified post"
     * )
     * @param Request $request
     * @param $postID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(Request $request, $postID) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $fileFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.file');
        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($postID);

        $content = $request->getContent();
        $params = json_decode($content, true);

        $file = new File();
        $file->setName($params['name']);
        $file->setData(null);
        $file->setPost($post);

        $fileFunc->create($file);

        $view = $this->view($file, 201);
        return $this->handleView($view);
    }

    /**
     * Delete file
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Delete file with specific ID",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($fileID) {

        /* Authentication */
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $fileFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.file');
        $file = $fileFunc->findById($fileID);

        /* Voter authorization */
        //$this->denyAccessUnlessGranted(PostVoter::POST_EDIT, $post, 'Nemáte oprávnění upravovat tento příspěvek!');

        $fileFunc->delete($file);

        $view = $this->view(null, 204);
        return $this->handleView($view);

    }

}