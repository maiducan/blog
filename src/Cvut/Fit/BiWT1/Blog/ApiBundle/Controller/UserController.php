<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 21.05.2016
 * Time: 8:58
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class UserController extends FOSRESTController implements ClassResourceInterface
{

    /**
     * Get user
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return user object",
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction() {
        
        $user = $this->get('security.context')->getToken()->getUser();

        if (!($user instanceof User)) {

            $user = new User();
            $user->setName("Guest");
            
        }

        $view = $this->view($user, 200);
        return $this->handleView($view);

    }

    /**
     * Get all users
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Returns all users in database"
     * )
     */
    public function cgetAction() {

        $userFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.user');
        $users = $userFunc->findAll();

        $view = $this->view($users, 200);
        return $this->handleView($view);
    }


}