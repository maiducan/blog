<?php
namespace Cvut\Fit\BiWT1\Blog\UiBundle\Filter;

/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 1/12/16
 * Time: 3:42 PM
 */
class Filter
{
    /**
     * @var string
     */
    private $author;

    /**
     * @var boolean
     */
    private $private;

    /**
     * @var array
     */
    private $tags;

    /**
     * @var \DateTime
     */
    private $publishFrom;

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return \DateTime
     */
    public function getPublishFrom()
    {
        return $this->publishFrom;
    }

    /**
     * @param \DateTime $publishFrom
     */
    public function setPublishFrom($publishFrom)
    {
        $this->publishFrom = $publishFrom;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }



}