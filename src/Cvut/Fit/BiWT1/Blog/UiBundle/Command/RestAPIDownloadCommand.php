<?php
/**
 * Created by PhpStorm.
 * User: maiducan
 * Date: 3/18/16
 * Time: 9:39 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Command;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp;
use JMS;


class RestAPIDownloadCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('restAPI:download')
            ->setDescription('restAPI functionality')
            ->addArgument(
                'url',
                InputArgument::REQUIRED
            )
        ;


    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');


        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        $type = $res->getHeaderLine('content-type');

        if (strpos($type, 'xml') !== false) $type = 'xml';
        if (strpos($type, 'json') !== false) $type = 'json';

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $object = $serializer->deserialize($res->getBody(), 'Cvut\Fit\BiWT1\Blog\BaseBundle\Rss\Rss', $type);


        $items = $object->getChannel()->getItem();

        $postOperation = $this->getContainer()->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $userFunc = $this->getContainer()->get('cvut_fit_biwt1_blog_base.service.functionality.user');
        $user = $userFunc->findById(8);

        foreach($items as $item) {

            $output->writeln($item->getTitle());

            $post = new Post();

            $post->setTitle($item->getTitle());
            $post->setText($item->getDescription());
            $post->setAuthor($user);
            $post->setCreated($item->getPubDate());

            $postOperation->create($post, [], [] );

        }

        $output->writeln($type);

    }

}