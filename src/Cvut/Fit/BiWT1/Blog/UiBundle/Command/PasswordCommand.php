<?php

/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 03.03.2016
 * Time: 15:38
 */
namespace Cvut\Fit\BiWT1\Blog\UiBundle\Command;

use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PasswordCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('setPassword')
            ->setDescription('set users password')
            ->addArgument(
                'id',
                InputArgument::REQUIRED
            )
            ->addArgument(
                'password',
                InputArgument::REQUIRED
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $new_password = $input->getArgument('password');

        /* Get the user from DB and change password */

        $userFunc = $this->getContainer()->get('cvut_fit_biwt1_blog_base.service.functionality.user');

        $user = $userFunc->findById($id);

        $encoder = $this->getContainer()->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $new_password);

        $user->setPassword($encoded);

        $userFunc->update($user);

        $output->writeln($user->getName() . " " . $user->getPassword());

    }
}
