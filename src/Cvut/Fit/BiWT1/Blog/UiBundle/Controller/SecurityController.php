<?php
/**
 * Created by PhpStorm.
<<<<<<< HEAD
 * User: ducan
 * Date: 03.03.2016
 * Time: 10:49
=======
 * User: musilane
 * Date: 15.02.16
 * Time: 17:35
>>>>>>> 8656b1a49e8e79f9212482a41b5945c87811286e
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('CvutFitBiWT1BlogUiBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }


}