<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/1/15
 * Time: 11:56 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Security\PostVoter;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\CommentType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\FilterType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Filter\Filter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class PostController extends Controller
{
        /**
         * @Route("/", name="home")
         */
        public function indexAction(Request $request)
        {
                $em = $this->getDoctrine()->getManager();
                $posts = $em->getRepository('CvutFitBiWT1BlogBaseBundle:Post')->findAll();

                $filter = new Filter();
                $form = $this->createForm(new FilterType(), $filter);
            

                $form->handleRequest($request);

                if ($form->isSubmitted()) {
                        if ($form->isValid()) {
                                $em = $this->getDoctrine()->getManager();
                                $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
                                if ($filter->getAuthor() != "")
                                        $posts = $postOperation->findByAuthor($filter->getAuthor());

                                if ($filter->isPrivate()) {
                                        $privatePosts = array();
                                        foreach ($posts as $post) {
                                                if ($post->isPrivate())
                                                        $privatePosts[] = $post;
                                        }
                                        $posts = $privatePosts;
                                }

                                return $this->render('CvutFitBiWT1BlogUiBundle:Post:index.html.twig', array('posts' => $posts,
                                    'form_filter' => $form->createView()));
                        }
                }

                $posts = $em->getRepository('CvutFitBiWT1BlogBaseBundle:Post')->getLatestPosts(10);
                $user = $this->getUser();

                return $this->render('CvutFitBiWT1BlogUiBundle:Post:index.html.twig', array(
                    'posts' => $posts,
                    'user' => $user,
                    'form_filter' => $form->createView(),
                ));
        }

        /**
         * @Route("/newpost", name="new_post")
         * @Template()
         */
        public function newAction(Request $request)
        {
                $post = new Post();

                $post->setCreated(new \DateTime());
                $post->setModified(new \DateTime());
                $form = $this->createForm(new PostType(), $post);

                $form->handleRequest($request);

                if ($form->isSubmitted()) {

                        if ($form["publishFrom"]->getData() > $form["publishTo"]->getData())
                                $form->addError(new FormError('publishFrom cannot be greater the publishTo'));
                        if ($form->isValid()) {
                                $em = $this->getDoctrine()->getManager();
                                $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
                                $tagTitles = array();
                                foreach ($form["tags"]->getData() as $tag) {
                                        $tagTitles[] = $tag->getTitle();
                                }
                                $postOperation->create($post, array(), $tagTitles);
                                return $this->redirect($this->generateUrl('post_detail', array('id' => $post->getId())));

                        }
                }

                return array('form_post' => $form->createView());
        }

        /**
         * @Route("/post/{id}/detail", name="post_detail")
         * @Template()
         */
        public function detailAction(Request $request, $id)
        {
                $em = $this->getDoctrine()->getManager();
                $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
                $post = $postOperation->findById($id);
                $latestComments = $em->getRepository('CvutFitBiWT1BlogBaseBundle:Comment')->getLatestComments($post->getId(), 10);


                /* Voter */
                $this->denyAccessUnlessGranted(PostVoter::POST_VIEW, $post, 'Nemáte oprávnění prohlížet tento příspěvek!');

                $comment = new Comment();
                $comment->setCreated(new \DateTime());
                $comment->setModified(new \DateTime());
                $comment->setPost($post);

                $form = $this->createForm(new CommentType(), $comment);

                $form->handleRequest($request);

                if ($form->isSubmitted()) {
                        if ($form->isValid()) {
                                $em = $this->getDoctrine()->getManager();
                                $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
                                $commentFunc->create($comment);
                                return $this->redirect($this->generateUrl('post_detail', array('id' => $post->getId())));
                        }
                }

                return $this->render('CvutFitBiWT1BlogUiBundle:Post:detail.html.twig',
                    array('post' => $post, 'comments' => $latestComments,
                        'form_comment' => $form->createView()));
        }

        /**
         * @Route("/post/{id}/delete", name="post_delete")
         * @Template()
         */
        public function deleteAction($id)
        {
                $em = $this->getDoctrine()->getManager();
                $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
                $post = $postOperation->findById($id);
                $postOperation->delete($post);
                return $this->redirectToRoute('home');
        }

        /**
         * @Route("/post/{id}/edit", name="post_edit")
         * @Template()
         */
        public function editAction(Request $request, $id)
        {
            $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
            $post = $postOperation->findById($id);

            /* Voter authorization */
            $this->denyAccessUnlessGranted(PostVoter::POST_EDIT, $post, 'Nemáte oprávnění upravovat tento příspěvek!');
                

            $form = $this->createForm(new PostType(), $post);
            $form->get('author')->setData($post->getAuthor());

            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                    if ($form["publishFrom"]->getData() > $form["publishTo"]->getData())
                            $form->addError(new FormError('publishFrom cannot be greater the publishTo'));
                    if ($form->isValid()) {
                            $post->setModified(new \DateTime());
                            $em = $this->getDoctrine()->getManager();
                            $em->flush();
                            return $this->redirectToRoute('home');
                    }
            }

            return array('form_post' => $form->createView());


        }
}
