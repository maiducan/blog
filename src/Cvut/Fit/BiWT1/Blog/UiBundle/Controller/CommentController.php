<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/1/15
 * Time: 11:56 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\CommentType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\FilterType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Filter\Filter;
use Cvut\Fit\BiWT1\Blog\UiBundle\CvutFitBiWT1BlogUiBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

class CommentController extends Controller
{
    /**
     * @Route("/comment/{id}/{post_id}/new", name="comment_new")
     * @Template()
     */
    public function newAction(Request $request, $id, $post_id)
    {
        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $parent = $commentFunc->findById($id);
        $postOperation = $this->container->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $postOperation->findById($post_id);

        $comment = new Comment();
        $form = $this->createForm(new CommentType(), $comment);
        $comment->setParent($parent);
        $comment->setPost($post);
        $comment->setCreated(new \DateTime());
        $comment->setModified(new \DateTime());

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            if($form->isValid())
            {
                $commentFunc->create($comment);
                return $this->redirect($this->generateUrl('post_detail', array('id'  => $comment->getPost()->getId())));
            }
        }

        return array('form_comment' => $form->createView());
    }

    /**
     * @Route("/comment/{id}/edit", name="comment_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $comment = $commentFunc->findById($id);
        $form = $this->createForm(new CommentType(), $comment);
        $form->get('author')->setData($comment->getAuthor());
        $form->get('text')->setData($comment->getText());

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            if($form->isValid())
            {
                $comment->setModified(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                return $this->redirect($this->generateUrl('post_detail', array('id'  => $comment->getPost()->getId())));
            }
        }

        return array('form_comment' => $form->createView());
    }

    /**
     * @Route("/comment/{id}/{post_id}/delete", name="comment_delete")
     * @Template()
     */
    public function deleteAction($id, $post_id)
    {
        $em = $this->getDoctrine()->getManager();
        $commentFunc = $this->container->get('cvut_fit_biwt1_blog_base.service.functionality.comment');
        $comment = $commentFunc->findById($id);
        $children = $comment->getChildren();
        foreach($children as $child) {
            $commentFunc->delete($child);
        }
        $commentFunc->delete($comment);
        return $this->redirect($this->generateUrl('post_detail', array('id'  => $comment->getPost()->getId())));
    }
}
