<?php
namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AngularController extends Controller
{
    /**
     * @Route("/")
     * @Template
     */
    public function indexAction()
    {

        $user = $this->getUser();

        return array('user' => $user);


    }
}