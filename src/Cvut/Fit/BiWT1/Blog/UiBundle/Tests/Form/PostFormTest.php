<?php
/**
 * Created by PhpStorm.
 * User: maiducan
 * Date: 3/11/16
 * Time: 9:46 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PostFormTest extends WebTestCase
{

    /** @var Client */
    protected $client = null;

    public function setUp()
    {
        parent::setUp();
        $this->client =  static::createClient([], [
            'HTTP_HOST' => 'localhost:8000'
        ]);
    }

    private function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        $firewall = 'secured_area';
        $token = new UsernamePasswordToken('admin', 'admin', $firewall, array('ROLE_ADMIN'));
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    public function testSecuredHello()
    {
        $this->logIn();

        $crawler = $this->client->request('GET', '/newpost');

        $status = $this->client->getResponse()->getStatusCode();

        dump($status);

        $form = $crawler->filter('button[type=submit]')->last()->form();

        $form['form_post[title]'] = 'TEST____CASE____TITLE';
        $form['form_post[text]'] = 'TEST____CASE____TEXT';

        $form['form_post[tags]'] = array('22');

        $this->client->submit($form);

        $status = $this->client->getResponse()->getStatusCode();

        dump($status);

        $crawler = $this->client->followRedirect();


        $title = $crawler->filter('h1')->last()->text();

        $this->assertEquals('TEST____CASE____TITLE', $title);



        /*$this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Admin Dashboard")')->count());*/
    }



}