<?php
namespace Cvut\Fit\BiWT1\Blog\UiBundle\Twig;

/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 1/12/16
 * Time: 3:13 PM
 */
class AuthorFilterExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function getName()
    {
        return 'app_extension';
    }
}