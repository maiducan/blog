<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/8/15
 * Time: 10:45 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('title', 'text', array('label' => 'Titulek', 'constraints' => array(
                new NotBlank())))
            ->add('text', 'text', array(
                'constraints' => new Length(array('min' => 3))))
            ->add('private', 'checkbox', array(
                'required' => FALSE))
            ->add('publishFrom')
            ->add('publishTo')
            ->add('author', 'entity', array(
                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User',
                'property' => 'name',
                'required' => FALSE // changed
            ))
            /*->add('tags', 'entity', array(
                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag',
                'property' => 'title',
                'multiple' => TRUE,
                'required' => FALSE // changed
            ))*/
            ->add('submit', 'submit');
            //->add('title', 'text', array('label' => 'Titulek'));
        // TODO - Add the rest of items.

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_post';
    }
}