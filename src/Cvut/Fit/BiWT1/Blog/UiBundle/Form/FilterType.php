<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 1/12/16
 * Time: 3:37 PM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class FilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', 'text', array('label' => 'Author','required' => FALSE))
            ->add('private', 'checkbox', array(
                'required' => FALSE))
            ->add('publishFrom', 'datetime', array( 'format' => 'MM-yyyy'))
//            ->add('publishTo')
//            ->add('author', 'entity', array(
//                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User',
//                'property' => 'name',
//            ))
//            ->add('tags', 'entity', array(
//                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag',
//                'property' => 'title',
//                'multiple' => TRUE
//            ))
            ->add('submit', 'submit', array('label' => 'Filter'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_filter';
    }
}