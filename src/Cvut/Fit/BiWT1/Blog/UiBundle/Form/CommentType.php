<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/8/15
 * Time: 10:45 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', 'text', array(
                'constraints' => new Length(array('min' => 2))))
            /*->add('author', 'entity', array(
                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User',
                'property' => 'name',
            ))*/
            ->add('submit', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_comment';
    }
}