<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 03.03.16
 * Time: 10:16
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Security;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;

class PostVoter extends AbstractVoter
{
    const POST_VIEW = 'view';
    const POST_CREATE = 'create';
    const POST_EDIT = 'edit';


    protected function getSupportedAttributes()
    {
        return array(self::POST_VIEW, self::POST_CREATE, self::POST_EDIT);
    }

    protected function getSupportedClasses()
    {
        return array('Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post');
    }

    protected function isGranted($attribute, $post, $user = null)
    {


        // make sure there is a user object (i.e. that the user is logged in)
        /*if (!$user instanceof UserInterface) {
            return false;
        }*/

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        /*if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }*/

        switch($attribute) {
            case self::POST_VIEW:
                // the data object could have for example a method isPrivate()
                // which checks the Boolean attribute $private
                if (!$post->isPrivate()) return true;
                if ($user->getRoles()[0] == 'ROLE_READER') return true;
                if ($user->getRoles()[0] == 'ROLE_AUTHOR') return true;
                if ($user->getRoles()[0] == 'ROLE_ADMIN') return true;


                break;

            case self::POST_CREATE:

                if ($user->getRoles()[0] == 'ROLE_AUTHOR') return true;
                if ($user->getRoles()[0] == 'ROLE_ADMIN') return true;

                break;

            case self::POST_EDIT:

                // this assumes that the data object has a getOwner() method
                // to get the entity of the user who owns this data object

                if ($user->getId() === $post->getAuthor()->getId()) return true;
                if ($user->getRoles()[0] == 'ROLE_ADMIN') return true;

                break;
        }



        return false;

    }

}