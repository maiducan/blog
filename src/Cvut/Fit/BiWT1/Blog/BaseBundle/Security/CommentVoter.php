<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 01.06.2016
 * Time: 6:34
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;

class CommentVoter extends AbstractVoter
{

    const COMMENT_CREATE = 'create';
    const COMMENT_EDIT = 'edit';

    protected function getSupportedAttributes()
    {
        return array(self::COMMENT_CREATE, self::COMMENT_EDIT);
    }

    protected function getSupportedClasses()
    {
        return array('Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment');
    }

    protected function isGranted($attribute, $comment, $user = null)
    {


        // make sure there is a user object (i.e. that the user is logged in)
        /*if (!$user instanceof UserInterface) {
            return false;
        }*/

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        /*if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }*/

        switch($attribute) {

            case self::COMMENT_CREATE:

                if ($user->getRoles()[0] == 'ROLE_READER') return true;
                if ($user->getRoles()[0] == 'ROLE_AUTHOR') return true;
                if ($user->getRoles()[0] == 'ROLE_ADMIN') return true;

                break;

            case self::COMMENT_EDIT:

                // this assumes that the data object has a getOwner() method
                // to get the entity of the user who owns this data object

                if ($user->getId() === $comment->getAuthor()->getId()) return true;
                if ($user->getRoles()[0] == 'ROLE_ADMIN') return true;

                break;
        }



        return false;

    }

}