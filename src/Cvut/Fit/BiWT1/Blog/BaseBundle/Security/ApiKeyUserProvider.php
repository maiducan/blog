<?php
/**
 * Created by PhpStorm.
 * User: ducan
 * Date: 08.04.2016
 * Time: 7:43
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Security;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\UserFunctionality;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiKeyUserProvider implements UserProviderInterface
{

    private $mUserFunctionality;


    public function setUserFunctionality (UserFunctionality $functionality) {
        $this->mUserFunctionality = $functionality;
    }

    public function loadUserByUsername($username)
    {

        if(!($user = $this->mUserFunctionality->findOneBy(array('name' => $username))))
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $username)
            );
        return $user;

    }

    public function loadUserByApiKey($key) {

        //$user = $this->mUserFunctionality->findByApiKey($key);

        if(!($user = $this->mUserFunctionality->findOneBy(array('apikey' => $key))))
            throw new UsernameNotFoundException(
                sprintf('API key "%s" does not exist.', $key)
            );

        return $user;

    }

    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Cvut\Fit\BiWt1\Blog\BaseBundle\Entity\User' === $class;
    }
}