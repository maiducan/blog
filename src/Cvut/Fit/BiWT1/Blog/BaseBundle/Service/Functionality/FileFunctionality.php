<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\File;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\File\FlysystemIntegration;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;

class FileFunctionality
{
    use FlysystemIntegration;

    /** @var FileRepository */
    protected $fileRepository;

    /**
     * @param FileRepository $fileRepository
     */
    public function setFileRepository($fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param File $file
     * @return array
     */
    public function retrieve($file)
    {
        return $this->retrieveFile($file->getName());
    }

    /**
     * @param File $file
     * @return File
     */
    public function create(File $file)
    {
        $file->setCreated(new \DateTime);
        $this->fileRepository->save($file);
        return $file;
    }

    /**
     * @param File $file
     * @return File
     * @throws ItemNotFoundException
     */
    public function update(File $file)
    {
        try {
            $this->fileRepository->save($file);
            return $file;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param File $file
     * @return File
     * @throws ItemNotFoundException
     */
    public function delete(File $file)
    {
        try {
            $this->fileRepository->delete($file);
            return $file;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return File
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $file = $this->fileRepository->findById($id);
        if($file instanceof File)
            return $file;

        throw new ItemNotFoundException();
    }

    /**
     * @return Collection<File>
     */
    public function findAll()
    {
        return $this->fileRepository->findAll();
    }

    /**
     * @param Post $post
     * @return Collection<File>
     */
    public function findByPost(Post $post)
    {
        $result = new ArrayCollection();
        $files = $this->findAll();

        /** @var Comment $comment */
        foreach($files as $file) {
            if ($file->getPost() == $post) {
                $result->add($file);
            }
        }
        return $result;
    }
}
