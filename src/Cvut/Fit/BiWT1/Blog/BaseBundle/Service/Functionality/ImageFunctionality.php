<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Doctrine\Common\Collections\ArrayCollection;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\PostRepository;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;

class ImageFunctionality
{
    /** @var PostRepository */
    protected $postRepository;

    /**
     * @param PostRepository $postRepository
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param Post $post
     * @return Post
     */
    public function create(Post $post)
    {
        $post->setCreated(new \DateTime);
        $this->postRepository->save($post);
        return $post;
    }

    /**
     * @param Post $post
     * @return Post
     * @throws ItemNotFoundException
     */
    public function update(Post $post)
    {
        try {
            $post->setModified(new \DateTime);
            $this->postRepository->save($post);
            return $post;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param Post $post
     * @return Post
     * @throws ItemNotFoundException
     */
    public function delete(Post $post)
    {
        try {
            $this->postRepository->delete($post);
            return $post;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return Post
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $post = $this->postRepository->findById($id);
        if($post instanceof Post)
            return $post;

        throw new ItemNotFoundException();
    }

    /**
     * @return Collection<Post>
     */
    public function findAll()
    {
        return $this->postRepository->findAll();
    }

    /**
     * @param Tag $tag
     * @return Collection<Post>
     */
    public function findByTag(Tag $tag)
    {
        $result = new ArrayCollection();
        $posts = $this->findAll();

        /** @var Post $post */
        foreach($posts as $post) {
            if ($post->getTags()->contains($tag)) {
                $result->add($post);
            }
        }
        return $result;
    }

    /**
     * @param User $user
     * @return Collection<Post>
     */
    public function findByAuthor(User $user)
    {
        return new ArrayCollection($this->postRepository->findBy(['author' => $user]));
    }
}
