<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Doctrine\Common\Collections\ArrayCollection;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\UserRepository;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;

class UserFunctionality
{
    /** @var UserRepository */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user)
    {
        $this->userRepository->save($user);
        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws ItemNotFoundException
     */
    public function update(User $user)
    {
        try {
            $this->userRepository->save($user);
            return $user;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param User $user
     * @return User
     * @throws ItemNotFoundException
     */
    public function delete(User $user)
    {
        try {
            $this->userRepository->delete($user);
            return $user;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return User
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $user = $this->userRepository->findById($id);
        if($user instanceof User)
            return $user;

        throw new ItemNotFoundException();
    }

    public function findByApiKey($key) {
        $users = $this->userRepository->findAll();

        foreach($users as $user) {
            if ($user->getApiKey == $key) return $user;
        }

        return null;
    }

    /**
     * @return Collection<User>
     */
    public function findAll()
    {
        return $this->userRepository->findAll();
    }

    public function findBy($criteria)
    {
        return $this->userRepository->findBy($criteria);
    }

    public function findOneBy($criteria) {
        return $this->userRepository->findOneBy($criteria);
    }
}
