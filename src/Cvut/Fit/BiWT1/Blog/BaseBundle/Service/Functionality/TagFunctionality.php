<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Doctrine\Common\Collections\ArrayCollection;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\TagRepository;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;

class TagFunctionality
{
    /** @var TagRepository */
    protected $tagRepository;

    /**
     * @param TagRepository $tagRepository
     */
    public function setTagRepository($tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param Tag $tag
     * @return Tag
     */
    public function create(Tag $tag)
    {
        $this->tagRepository->save($tag);
        return $tag;
    }

    /**
     * @param Tag $tag
     * @return Tag
     * @throws ItemNotFoundException
     */
    public function update(Tag $tag)
    {
        try {
            $this->tagRepository->save($tag);
            return $tag;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param Tag $tag
     * @return Tag
     * @throws ItemNotFoundException
     */
    public function delete(Tag $tag)
    {
        try {
            $this->tagRepository->delete($tag);
            return $tag;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return Tag
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $tag = $this->tagRepository->findById($id);
        if($tag instanceof Tag)
            return $tag;

        throw new ItemNotFoundException();
    }

    /**
     * @return Collection<Tag>
     */
    public function findAll()
    {
        return $this->tagRepository->findAll();
    }

    /**
     * @param Post $post
     * @return Collection<Tag>
     */
    public function findByPost(Post $post)
    {
        $result = new ArrayCollection();
        $tags = $this->findAll();

        /** @var Tag $tag */
        foreach($tags as $tag) {
            if ($tag->getPosts()->contains($post)) {
                $result->add($tag);
            }
        }
        return $result;
    }
}
