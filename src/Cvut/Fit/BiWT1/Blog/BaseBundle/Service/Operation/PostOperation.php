<?php
/**
 * Created by PhpStorm.
 * User: jirkovoj
 * Date: 24/11/15
 * Time: 18:56
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Operation;


use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\File;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\TagFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\PostFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\FileFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\UserFunctionality;


class PostOperation
{
	/** @var TagFunctionality */
	protected $tagFunctionality;

	/** @var PostFunctionality */
	protected $postFunctionality;

	/** @var FileFunctionality */
	protected $fileFunctionality;

	/** @var UserFunctionality */
	protected $userFunctionality;

	/**
	 * @param integer $id
	 * @return Post
	 */
	public function findById($id)
	{
		return $this->postFunctionality->findById($id);
	}

	public function findByTag($tagTitle)
	{

	}

	/**
	 * @param Post $post
	 */
	public function delete(Post $post)
	{
		$files = $post->getFiles();
		foreach ($files as $file) {
			$this->fileFunctionality->delete($file);
		}
		$this->postFunctionality->delete($post);
	}

	/**
	 * @return Collection<Post>
	 */
	public function getLatestPosts()
	{
		return $this->postFunctionality->getLatestPosts();
	}

	/**
	 * @return Collection<Post>
	 */
	public function getAllPosts() {

		return $this->postFunctionality->findAll();

	}

	/**
	 * @param \DateTime $lastDate
	 * @return Collection<Post>
	 */
	public function getPrevPosts($lastDate)
	{
		return $this->postFunctionality->getPrevPosts($lastDate);
	}

	public function findByAuthor($authorName)
	{
		$criteria = array('name' => $authorName);
		/** @var User $author */
		$author = $this->userFunctionality->findBy($criteria);
		return $this->postFunctionality->findByAuthor($author[0]);
	}


	public function update(Post $post) {

		$this->postFunctionality->update($post);

	}

	 /** @param Post $post
	 * @param array $files
	 * @param array $tags
	 */
	public function create(Post $post, $files, $tags)
	{

		foreach ($files as $key => $value) {
//			printf("HERE: $files[$key]\n");
			$file = new File();
			$file->setName($key);
			$file->setData("");
			$file->setInternetMediaType("");
			$file->setPost($post);
			$this->fileFunctionality->create($file);
			$post->addFile($file);
			$this->fileFunctionality->createFile($key, $value);
		}

		/** @var  Collection<Tag> */
		$existTags = $this->tagFunctionality->findAll();
		foreach($tags as $tagName) {
			$exist = false;
			foreach ($existTags as $existTag) {
				if ($existTag->getTitle() == $tagName) {
					$existTag->addPost($post);
					$this->tagFunctionality->update($existTag);
					$post->addTag($existTag);
					$exist = true;
					break;
				}
			}
			if (!$exist) {
				$tag = new Tag();
				$tag->setTitle($tagName);
				$tag->addPost($post);
				$this->tagFunctionality->create($tag);
				$post->addTag($tag);
			}

		}
		printf("Tags:\n");
		foreach($tags as $tagName) {
			printf("$tagName\n");
		}

		$this->postFunctionality->create($post);
	}

	/**
	 * @param TagFunctionality $tagFunctionality
	 */
	public function setTagFunctionality($tagFunctionality)
	{
		$this->tagFunctionality = $tagFunctionality;
	}

	/**
	 * @param PostFunctionality $postFunctionality
	 */
	public function setPostFunctionality($postFunctionality)
	{
		$this->postFunctionality = $postFunctionality;
	}

	/**
	 * @param FileFunctionality $fileFunctionality
	 */
	public function setFileFunctionality($fileFunctionality)
	{
		$this->fileFunctionality = $fileFunctionality;
	}

	/**
	 * @param UserFunctionality $userFunctionality
	 */
	public function setUserFunctionality($userFunctionality)
	{
		$this->userFunctionality = $userFunctionality;
	}


}