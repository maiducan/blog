<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 11/19/15
 * Time: 1:08 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

class ImageRepository extends EntityRepository
{
    /**
     * @param int $id
     * @return Image
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Image $image
     */
    public function save(Image $image)
    {
        $this->getEntityManager()->persist($image);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Image $image
     */
    public function delete(Image $image)
    {
        $this->getEntityManager()->remove($image);
        $this->getEntityManager()->flush();
    }

    /**
     * @return Collection<Image>
     */
    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}