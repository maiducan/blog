<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 11/19/15
 * Time: 1:05 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;


class FileRepository extends EntityRepository
{
    /**
     * @param int $id
     * @return File
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param File $file
     */
    public function save(File $file)
    {
        $this->getEntityManager()->persist($file);
        $this->getEntityManager()->flush();
    }

    /**
     * @param File $file
     */
    public function delete(File $file)
    {
        $this->getEntityManager()->remove($file);
        $this->getEntityManager()->flush();
    }

    /**
     * @return Collection<File>
     */
    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}