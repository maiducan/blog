<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 11/19/15
 * Time: 1:06 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

class CommentRepository extends EntityRepository
{
    /**
     * @param int $id
     * @return Comment
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Comment $comment
     */
    public function save(Comment $comment)
    {
        $this->getEntityManager()->persist($comment);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Comment $comment
     */
    public function delete(Comment $comment)
    {
        $this->getEntityManager()->remove($comment);
        $this->getEntityManager()->flush();
    }

    /**
     * @return Collection<Comment>
     */
    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    /**
     * @return array
     */
    public function getLatestComments($postId, $limit = null)
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('c')
            ->select('c, p')
            ->leftJoin('c.post', 'p')
            ->where('p.id = :postId')
            ->setParameter('postId', $postId)
            ->addOrderBy('c.created', 'DESC');

        if (false === is_null($limit))
            $qb->setMaxResults($limit);

        return $qb->getQuery()
            ->getResult();
    }
}