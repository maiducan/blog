<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 10/22/15
 * Time: 1:25 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Tag
 *
 * @ORM\Entity(repositoryClass="TagRepository")
 * @ORM\Table(name="blog_tag")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class Tag
{
    /**
     * Unikatni ID tagu
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    private $id;

    /**
     * title
     *
     * @ORM\Column(type="string")
     * @var string
     */
    private $title;

    /**
     * Kolekce prispevku
     * @ORM\ManyToMany(targetEntity="Post", inversedBy="tags",cascade={"persist"})
     * @var Collection<Post>
     */
    private $posts;

    /**
     * Tag constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

}