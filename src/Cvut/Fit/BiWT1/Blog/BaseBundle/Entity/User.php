<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 10/22/15
 * Time: 1:46 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as JMS;

/**
 * Class User
 *
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="blog_user")
 * @JMS\ExclusionPolicy("none")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class User implements UserInterface
{
    /**
     * Unikatni ID uzivatele
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    private $id;
    /**
     * Jmeno uzivatele
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * Heslo uzivatele
     * @ORM\Column(type="string", length=64, nullable=true)
     * @JMS\Exclude
     */
    private $password;

    /**
     * Prispevky uzivatele
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     * @var Collection<Post>
     * @JMS\Exclude
     */
    private $posts;

    /**
     * @var RoleInterface
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="users")
     */
    protected $role;

    /**
     * @ORM\Column(type="string")
     * @JMS\Exclude
     */
    protected $apikey;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost(Post $post)
    {
        $this->posts[] = $post;
    }

    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }


    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        $role = array($this->role->getRole());
        return $role;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->name;
    }

    public function getApiKey() {
        return $this->apikey;
    }

    public function setApiKey($key) {
        $this->apikey = $key;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}