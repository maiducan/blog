<?php

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class File
 *
 * @ORM\Entity(repositoryClass="FileRepository")
 * @ORM\Table(name="blog_file")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class File
{
	/**
	 * Unikatni ID souboru
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="bigint")
	 * @var number
	 */
	protected $id;
    /**
     * Nazev souboru
	 * @ORM\Column(type="string")
	 * @var string
     */
	protected $name;
    /**
     * Prislusny prispevek
	 * @ORM\ManyToOne(targetEntity="Post", inversedBy="files",cascade={"persist"})
     * @var Post
     */
	protected $post;
    /**
     * Prislusny komentar
	 * @ORM\ManyToOne(targetEntity="Comment", inversedBy="files")
     * @var Comment
     */
	protected $comment;
    /**
     * Cas vytvoreni souboru
	 * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $created;

    /**
     * MediaType
	 * @ORM\Column(type="string")
     * @var string
     */
    protected $internetMediaType;

    /**
     * Obsah souboru
	 * @ORM\Column(type="blob")
     *
     */
    protected $data;


	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getPost()
	{
		return $this->post;
	}

	/**
	 * @param mixed $post
	 */
	public function setPost($post)
	{
		$this->post = $post;
	}



	public function setComment(Comment $comment)
	{
		$this->comment = $comment;
	}

	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getInternetMediaType()
    {
        return $this->internetMediaType;
    }

    /**
     * @param string $internetMediaType
     */
    public function setInternetMediaType($internetMediaType)
    {
        $this->internetMediaType = $internetMediaType;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

	// TODO
}