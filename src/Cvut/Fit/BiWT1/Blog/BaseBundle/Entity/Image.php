<?php

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Image
 *
 * @ORM\Entity(repositoryClass="ImageRepository")
 * @ORM\Table(name="blog_image")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class Image extends File
{
    /**
     * RosliseniX
     * @ORM\Column(type="integer")
     * @var int
     */
    private $dimensionX;
    /**
     * RosliseniY
     * @ORM\Column(type="integer")
     * @var int
     */
    private $dimensionY;
    /**
     * Prohlidka obrazku
     * @ORM\Column(type="blob") // Nemelo by byt string
     * @var string
     */
    private $preview;

    /**
     * @return mixed
     */
    public function getDimensionX()
    {
        return $this->dimensionX;
    }

    /**
     * @param mixed $dimensionX
     */
    public function setDimensionX($dimensionX)
    {
        $this->dimensionX = $dimensionX;
    }

    /**
     * @return string
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param string $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
    }

    /**
     * @return mixed
     */
    public function getDimensionY()
    {
        return $this->dimensionY;
    }

    /**
     * @param mixed $dimensionY
     */
    public function setDimensionY($dimensionY)
    {
        $this->dimensionY = $dimensionY;
    }

}