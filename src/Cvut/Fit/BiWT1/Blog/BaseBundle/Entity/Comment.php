<?php

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Comment
 *
 * @ORM\Entity(repositoryClass="CommentRepository")
 * @ORM\Table(name="blog_comment")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 * @JMS\ExclusionPolicy("all")
 */
class Comment
{
    /**
     * Unikatni ID komentaru
     * @JMS\Expose
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
	private $id;
    /**
     * Obsah komentaru
     * @ORM\Column(type="string")
     * @var string
     * @JMS\Expose
     */
	private $text;
    /**
     * Prislusny post
     * @ORM\ManyToOne(targetEntity="Post",inversedBy="comments")
     * @var Post
     */
	private $post;
    /**
     * Prislusny soubory
     * @ORM\OneToMany(targetEntity="File", mappedBy="comment")
     * @var Collection<File>
     */
	private $files;

    /**
     * Autor komentaru
     * @ORM\ManyToOne(targetEntity="User")
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * @var User
     */
    private $author;
    /**
     * Rodicovsky komentar
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy = "children")
     * @var Comment
     * @JMS\Expose  
     */
    private $parent;

    /**
     * Potomci
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     * @var Collection<Comment>
     */
    private $children;

    /**
     * Cas vytvoreni
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @var \DateTime
     */
    private $created;

    /**
     * Cas editovani
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $modified;

    /**
     * isSpam
     * @var boolean
     */
    private $spam;

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return boolean
     */
    public function isSpam()
    {
        return $this->spam;
    }

    /**
     * @param boolean $spam
     */
    public function setSpam($spam)
    {
        $this->spam = $spam;
    }

    /**
     * @return Post
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }


    function __construct()
    {
        $this->files = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @param Post $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @param mixed $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}

	/**
	 * @return mixed
	 */
	public function getPost()
	{
		return $this->post;
	}

	/**
	 * @param mixed $post
	 */
	public function setPost($post)
	{
		$this->post = $post;
	}

	/**
	 * @return mixed
	 */
	public function getFiles()
	{
		return $this->files;
	}

    public function addFile(File $file)
    {
        $this->files[] = $file;
    }

    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    public function addChild(Comment $comment)
    {
        $this->children[] = $comment;
    }

    public function removeChild(Comment $comment)
    {
        $this->children->removeElement($comment);
    }
}