<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:00 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    use CRUD;

    /**
     * @return array
     */
    public function getLatestPosts($limit = null)
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('posts')
            ->select('posts, c')
            ->leftJoin('posts.comments', 'c')
            ->where('posts.publishFrom <= :now')
            ->andWhere('posts.publishTo >= :now')
            ->setParameter('now', $now->format('Y-m-d H:i:s'))
            ->addOrderBy('posts.created', 'DESC');

        if (false === is_null($limit))
            $qb->setMaxResults($limit);

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTime $lastDate
     * @return array
     */
    public function getPrevPosts($lastDate, $limit = null)
    {
        $qb = $this->createQueryBuilder('posts')
            ->select('posts, c')
            ->leftJoin('posts.comments', 'c')
            ->where('posts.created > :lastDate')
            ->setParameter('lastDate', $lastDate->format('Y-m-d'))
            ->addOrderBy('posts.created', 'DESC');

        if (false === is_null($limit))
            $qb->setMaxResults($limit);

        return $qb->getQuery()
            ->getResult();
    }

}