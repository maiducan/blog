<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Post
 *
 * @ORM\Entity(repositoryClass="PostRepository")
 * @ORM\Table(name="blog_post")
 * @JMS\ExclusionPolicy("none")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class Post
{
    /**
     * Unikatni ID prispevku
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;

    /**
     * Titulek prispevku
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title;

    /**
     * Text (obsah) prispevku
     * @ORM\Column(type="string")
     * @var string
     */
    protected $text;

    /**
     * Autor prispevku
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * @var User
     */
    protected $author;

    /**
     * Soukromi prispevku
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    protected $private = false;

    /**
     * Datum vytvoreni prispevku
     * @ORM\Column(type="datetime", nullable=true)
     * @var \Datetime
     */
    protected $created;

    /**
     * Datum posledni zmeny prispevku
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $modified;

    /**
     * Viditelnost prispevku (od)
     * @ORM\Column(type="datetime", nullable=true)
     * @var \Datetime
     */
    protected $publishFrom;

    /**
     * Viditelnost prispevku (do)
     * @ORM\Column(type="datetime", nullable=true)
     * @var \Datetime
     */
    protected $publishTo;

    /**
     * Kolekce komentaru prispevku
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post", cascade={"remove"})
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post")
     * @JMS\MaxDepth(1)
     * @var Collection<Comment>
     */
    protected $comments;

    /**
     * Kolekce souboru prispevku
     * @ORM\OneToMany(targetEntity="File", mappedBy="post", cascade={"remove"})
     * @var Collection<File>
     */
    protected $files;

    /**
     * Kolekce tagu vztahujici se k prispevku
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Tag", mappedBy="posts")
     * @ORM\JoinTable(name="blog_post_tag")
     * @JMS\Exclude
     */
    protected $tags;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;
    }

    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
    }

    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    public function addFile(File $file)
    {
        $this->files[] = $file;
    }

    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return \Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return \Datetime
     */
    public function getPublishFrom()
    {
        return $this->publishFrom;
    }

    /**
     * @param \Datetime $publishFrom
     */
    public function setPublishFrom($publishFrom)
    {
        $this->publishFrom = $publishFrom;
    }

    /**
     * @return \Datetime
     */
    public function getPublishTo()
    {
        return $this->publishTo;
    }

    /**
     * @param \Datetime $publishTo
     */
    public function setPublishTo($publishTo)
    {
        $this->publishTo = $publishTo;
    }

    /**
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }



}

