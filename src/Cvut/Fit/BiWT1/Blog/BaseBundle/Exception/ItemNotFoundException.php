<?php
/**
 * Created by PhpStorm.
 * User: alsid
 * Date: 10/22/15
 * Time: 1:49 PM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Exception;

use Exeption;

class ItemNotFoundException extends \Exception
{
    public function __construct($message = "Item not found.", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}